/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import admitereulbs.Users;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Cristi
 */
public class Log extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Login</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Login at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String pass = request.getParameter("password");
        Boolean remember = Boolean.parseBoolean(request.getParameter("cookieOn"));
        if (email != null && !email.isEmpty() && email != null && !email.isEmpty()) {
            Boolean existsEmail = false;
            // check if user exists

            EntityManagerFactory emf = Persistence.createEntityManagerFactory("AdmitereUlbsModelPU");
            EntityManager em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction(); 
            tx.begin();

            TypedQuery<Users> query = em.createNamedQuery("Users.findAll", Users.class);
            List<Users> results = query.getResultList();

            for (int i = 0; i < results.size(); i++) {
                Users usr=(Users)((Object)results.get(i));
                if (usr.getEmail().compareTo(email) == 0 && usr.getPassword().compareTo(pass) == 0) 
                {
                    if (remember) {
                        request.login(usr.getEmail(), usr.getPassword()); // Password should already be the hashed variant.
                        

                        if (remember) {
                            //  String uuid = UUID.randomUUID().toString();
                            // rememberDAO.save(uuid, user);
                            addCookie(response, "user_autentificat", usr.getIdUser().toString(), 999);
                        } else {
                            //rememberDAO.delete(user);
                            //removeCookie(response, COOKIE_NAME);
                        }
                    }
                    
                    HttpSession sess= request.getSession();
                    sess.setAttribute("user", usr);
                   tx.commit();
                    em.close();
                    emf.close();
                    response.sendRedirect("View/User/UserPage.jsp");
                    return;
                }
                
            }
                

            tx.commit();
            em.close();
            emf.close();
response.sendRedirect("View/Index.jsp");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static String getCookieValue(HttpServletRequest request, String name) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (name.equals(cookie.getName())) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }

    public static void addCookie(HttpServletResponse response, String name, String value, int maxAge) {
        Cookie cookie = new Cookie(name, value);
        cookie.setPath("/");
        cookie.setMaxAge(maxAge);
        response.addCookie(cookie);
    }

    public static void removeCookie(HttpServletResponse response, String name) {
        addCookie(response, name, null, 0);
    }
}
