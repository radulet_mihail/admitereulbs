/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import admitereulbs.Users;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Cristi
 */
@WebServlet(urlPatterns = {"/Register"})
public class Register extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Register</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Register at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        String name,surname,email,password,phoneNumber,fatherName,motherName;
        double grade=0,admissionGrade=0;
        Boolean isOlympic=false; 
        grade=Double.parseDouble(request.getParameter("grade"));
        admissionGrade=Double.parseDouble(request.getParameter("admissionGrade"));
        //isOlympic=Boolean.parseBoolean(request.getParameter("isOlympic"));
        isOlympic= true;
        name=request.getParameter("name");
        surname=request.getParameter("surname");
        email=request.getParameter("email");
        password=request.getParameter("password");
        phoneNumber=request.getParameter("phoneNumber");
        fatherName=request.getParameter("fatherName");
        motherName=request.getParameter("motherName");
        if(grade>0 || admissionGrade>0 || name != null || !name.isEmpty()|| fatherName != null || !fatherName.isEmpty()|| motherName != null || !motherName.isEmpty()||surname != null || !surname.isEmpty()|| email != null || !email.isEmpty()|| password != null || !password.isEmpty()|| phoneNumber != null || !phoneNumber.isEmpty()){
            
        
            
        //password must be encoded in md5 + personalized keys
        
        
        //create a user
        //append it to the users list and add it to the  USER LIST AND REDIRECT IT to the login page
       
        
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("AdmitereUlbsModelPU");
        EntityManager em = emf.createEntityManager();
        

        em.getTransaction().begin();
        
        Users us = new Users();
        us.setName(name);
        us.setSurname(surname);
        us.setEmail(email);
        us.setPassword(password);
        
        //etc
        em.persist(us);
        

        em.getTransaction().commit();
         
        em.close();
        emf.close();
        
        } 
        return ;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    boolean tryParseInt(String value) {  
     try {  
         Integer.parseInt(value);  
         return true;  
      } catch (NumberFormatException e) {  
         return false;  
      }  
}
}
