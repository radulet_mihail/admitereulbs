/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import admitereulbs.Faculty;
import admitereulbs.University;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mihail
 */
@WebServlet(urlPatterns = {"/AddUniversity"})
public class AddUniversity extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddUniversity</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddUniversity at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
      processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            
        
       String name,rector,address,about;
     name=request.getParameter("name");
     rector=request.getParameter("rector");
     address=request.getParameter("address");
     about=request.getParameter("about");
     if(!(name==null && rector==null && address==null && about==null))
     {
          EntityManagerFactory emf = Persistence.createEntityManagerFactory("AdmitereUlbsModelPU");
          EntityManager em = emf.createEntityManager();
          University univ =new University();
          em.getTransaction().begin();

          //univ.setIdFaculty(12); // Implementeaza sa ia de pe UI ce  idFaculty este selectat SAU taie din DB IdFaculty din tabela University 
          univ.setName(name);
          univ.setAddress(address);
          univ.setRector(rector);
          univ.setAbout(about);
          
          em.persist(univ);

          em.getTransaction().commit();
          em.close();
          emf.close();
          response.sendRedirect("View/Admin/AdminIndex.jsp");
     }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
