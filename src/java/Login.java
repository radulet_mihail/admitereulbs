/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import admitereulbs.Users;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Cristi
 */
@WebServlet(urlPatterns = {"/Login"})
public class Login extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Login</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Login at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String pass = request.getParameter("password");

        Boolean existsEmail = false;
        // check if user exists
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("AdmitereUlbsModelPU");
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();

        TypedQuery<Users> query = em.createNamedQuery("Users.findAll", Users.class);
        List<Users> results = query.getResultList();
        
        for(int i=0;i<results.size() ;i++){
           if(results.get(i).getEmail().compareTo(email)== 0){
               existsEmail =true;
           }
        }
        
        
        if (existsEmail) {
            //return the response
        } else {
            // add keys and convert
            Boolean correctPassword = false;
            
           for(int i=0;i<results.size() ;i++){
           if(results.get(i).getPassword().compareTo(pass)== 0){
               correctPassword =true;
            }
            }
            
            if (correctPassword) {
                //iff coockie is checked => create a cookie with 1 mth live
                //redirect to the user page
            } else 
            {
                //return response password is incorrect
            }

        }
        
        em.getTransaction().commit();       
        em.close();
        emf.close();

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
