/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import admitereulbs.Users;
import admitereulbs.PrivateDatas;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Cristi
 */
@WebServlet(urlPatterns = {"/Reg"})
public class Reg extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Register</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Register at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        String name,repass,surname,email,password,phoneNumber,fatherName,motherName,address,identityNumber;
        float grade=0,admissionGrade=0;
        Boolean isOlympic=false; 
        //grade=Float.parseFloat(request.getParameter("grade"));
        //admissionGrade=Float.parseFloat(request.getParameter("admissionGrade"));
        //isOlympic=Boolean.parseBoolean(request.getParameter("isOlympic"));
        isOlympic= true;
        name=request.getParameter("name");
        surname=request.getParameter("surname");
        email=request.getParameter("email");
        password=request.getParameter("password");
        repass=request.getParameter("repPassword");
//        identityNumber=request.getParameter("identityNumber");
//        phoneNumber=request.getParameter("phoneNumber");
//        fatherName=request.getParameter("fatherName");
//        motherName=request.getParameter("motherName");
//         address=request.getParameter("address");
        
        
        
        if( password!=null && !password.isEmpty() && repass!=null && !repass.isEmpty() && password.equals(repass) && name!=null && !name.isEmpty() && surname!=null && !surname.isEmpty() && email!=null && !email.isEmpty())
        {
            
        
            
        //password must be encoded in md5 + personalized keys
        
        
        //create a user
        //append it to the users list and add it to the  USER LIST AND REDIRECT IT to the login page
       
        
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("AdmitereUlbsModelPU");
        EntityManager em = emf.createEntityManager();
        

        em.getTransaction().begin();
     //   PrivateDatas data= new PrivateDatas();
        
//        data.setAddress(address);
//        data.setFatherName(fatherName);
//        data.setMotherName(motherName);
//        data.setGrade(grade);
//        data.setAdmisionGrade(admissionGrade);
//        data.setIdentityCode(identityNumber);
//        data.setPhoneNumber(phoneNumber);
//        data.setBirthdate(new Date());
//        data.setIsOlympic(false);
  
        
                
        Users us = new Users();
        
        us.setName(name);
        us.setSurname(surname);
        us.setEmail(email);
        us.setPassword(password);
        //us.setRegDate(new Date());                Error -> Implementeaza setRegDate
        
        
        
        //etc
    em.persist(us);
    
      // data.setIdUser(us.getIdUser()); 
     //  em.persist(data);
        em.getTransaction().commit();
         
        em.close();
        emf.close();
         response.sendRedirect("View/login.jsp");
        } 
         
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    boolean tryParseInt(String value) {  
     try {  
         Integer.parseInt(value);  
         return true;  
      } catch (NumberFormatException e) {  
         return false;  
      }  
}
}
