/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import admitereulbs.PrivateDatas;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mihail
 */
@WebServlet(urlPatterns = {"/AddPrivateDatas"})
public class AddPrivateDatas extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddPrivateDatas</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddPrivateDatas at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
      processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                  
     String identityCode,address,motherName,fatherName,phoneNumber;
     float grade,admisionGrade; 
     int idUser,olympic;
     boolean isOlympic;
     Date  birthDate = new Date ();

    String input = request.getParameter("birthDate");
    SimpleDateFormat parser = new SimpleDateFormat("dd-MMM-yyy");
    try {
            birthDate= parser.parse(input);
    } catch (ParseException ex) {
            Logger.getLogger(AddPrivateDatas.class.getName()).log(Level.SEVERE, null, ex);
    }
        
     identityCode = request.getParameter("identityCode");
     address=request.getParameter("address");
     motherName=request.getParameter("motherName");
     fatherName=request.getParameter("fatherName");
     grade= Float.parseFloat (request.getParameter("grade"));
     admisionGrade = Float.parseFloat (request.getParameter("admisionGrade"));
     phoneNumber = request.getParameter("phoneNumber");
     
     idUser = 1 ;          //ID USER
     
     olympic= Integer.parseInt(request.getParameter("isOlympic"));
     if(olympic == 0){
         isOlympic = false ;
     }
     else {
         isOlympic = true;
     }
     
     if(!(identityCode==null && address==null && address==null && motherName==null && fatherName==null && grade== 0 && admisionGrade==0 && idUser==0 )) 
     {
          EntityManagerFactory emf = Persistence.createEntityManagerFactory("AdmitereUlbsModelPU");
          EntityManager em = emf.createEntityManager();
          PrivateDatas pd =new PrivateDatas();
          em.getTransaction().begin();

          pd.setAddress(address);
          pd.setAdmisionGrade(admisionGrade);
          pd.setBirthdate(birthDate);
          pd.setFatherName(fatherName);
          pd.setGrade(grade);
          pd.setIdUser(idUser);
          pd.setIdentityCode(identityCode);
          pd.setIsOlympic(isOlympic);
          pd.setMotherName(motherName);
          pd.setPhoneNumber(phoneNumber);
          
          
          em.persist(pd);

          em.getTransaction().commit();
          em.close();
          emf.close();
          response.sendRedirect("View/Admin/AdminIndex.jsp");
     }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
