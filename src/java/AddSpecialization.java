/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import admitereulbs.Faculty;
import admitereulbs.Specialization;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Cristi
 */
public class AddSpecialization extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddSpecialization</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddSpecialization at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
            processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
  
       String name;
       int idFaculty=0,numberOfSemester=0,paidPlaces=0,sponsoredPlaces=0;
       Boolean hasEntryExam=false;
       Date examDate= new Date();
     name=request.getParameter("name");
     idFaculty=Integer.parseInt(request.getParameter("idFaculty"));
     numberOfSemester=Integer.parseInt(request.getParameter("numberOfSemester"));
     paidPlaces=Integer.parseInt(request.getParameter("paidPlaces"));
     sponsoredPlaces=Integer.parseInt(request.getParameter("sponsoredPlaces"));
     
   
     if(idFaculty>0)
     {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("AdmitereUlbsModelPU");
            EntityManager em = emf.createEntityManager();
             Specialization spec=new Specialization();
            em.getTransaction().begin();

            spec.setIdFaculty(idFaculty);
            spec.setName(name);
            spec.setNumberOfSemester((short)numberOfSemester);
            spec.setExamDate(examDate);
            spec.setHasEntryExam(hasEntryExam);
            spec.setPaidPlaces(paidPlaces);
            spec.setSponsoredPlaces(sponsoredPlaces);
          
          
           em.persist(spec);

            em.getTransaction().commit();
            em.close();
            emf.close();
            response.sendRedirect("View/Admin/AdminIndex.jsp");
     }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
