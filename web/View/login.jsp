<%-- 
    Document   : login
    Created on : Nov 18, 2017, 2:17:48 PM
    Author     : Cristi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="Partials/header.jsp"/>
<div class="space-medium">
    <div class="container">
        <div class="row">
            <div class="offset-lg-3 offset-md-3  offset-sm-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="account-holder">
                    <!--login-form-->
                    
                    <h3>Login</h3>
                    <br>
                    <div class="social-btn">
                        <h6>Sign in With</h6>
                        <div class="fb-btn">
                            <i class="fa fa-facebook-official"></i><a href="#" class="fb-btn-text">facebook</a>
                        </div>
                        <div class="google-btn">
                            <i class="fa fa-google"></i><a href="#" class="google-btn-text">Google</a>
                        </div>
                    </div>
                    <div class="row">
                        <form  method="post" action="../Log">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="email">Email<sup style="color:red">*</sup></label>
                                    <input id="email" name="email" type="text" class="form-control" placeholder="Enter Email Address">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="password">Password<sup style="color:red">*</sup></label>
                                    <input id="password" name="password" type="password" class="form-control" placeholder="password">
                                </div>
                                <a href="#" class="forgot-password">Forgot Password?</a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button class="btn btn-primary btn-block" type="submit" >Login</button>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="cookieOn" value="">

                                        <span>Remember Me?</span>
                                    </label>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--/.login-form-->
          
          
        </div>

    </div>
</div>

<jsp:include page="Partials/footer.jsp"/>