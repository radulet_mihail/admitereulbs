<%-- 
    Document   : Loader
    Created on : Nov 18, 2017, 2:50:36 PM
    Author     : Cristi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link href="${pageContext.request.contextPath}/Resources/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="${pageContext.request.contextPath}/Resources/js/bootstrap.min.js" type="text/javascript"></script>
<link href="${pageContext.request.contextPath}/Resources/css/site.css" rel="stylesheet" type="text/css"/>
<link href="${pageContext.request.contextPath}/Resources/css/profile.css" rel="stylesheet" type="text/css"/>