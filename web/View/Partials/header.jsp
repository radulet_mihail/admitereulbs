<%-- 
    Document   : header
    Created on : Nov 11, 2017, 5:28:52 PM
    Author     : Cristi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <title>Admitere ULBS</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <jsp:include page="Loader.jsp"/>

    <body id="myPage">

        <!-- Sidebar on click -->
        <nav class="w3-sidebar w3-bar-block w3-white w3-card w3-animate-left w3-xxlarge" style="display:none;z-index:2" id="mySidebar">
            <a href="javascript:void(0)" onclick="w3_close()" class="w3-bar-item w3-button w3-display-topright w3-text-teal">Close
                <i class="fa fa-remove"></i>
            </a>
            <a href="#" class="w3-bar-item w3-button">Link 1</a>
            <a href="#" class="w3-bar-item w3-button">Link 2</a>
            <a href="#" class="w3-bar-item w3-button">Link 3</a>
            <a href="#" class="w3-bar-item w3-button">Link 4</a>
            <a href="#" class="w3-bar-item w3-button">Link 5</a>
        </nav>

        <!-- Navbar -->
        <div class="w3-top">
            <div class="w3-bar w3-theme-d2 w3-left-align">
                <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-hover-white w3-theme-d2" href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars"></i></a>
                <a href="Index.jsp" class="w3-bar-item w3-button w3-teal"><i class="fa fa-tripadvisor w3-margin-right"></i>ULBS</a>
                <a href="#team" class="w3-bar-item w3-button w3-hide-small w3-hover-white">Apply</a>
                <a href="#work" class="w3-bar-item w3-button w3-hide-small w3-hover-white">About</a>
                <a href="#pricing" class="w3-bar-item w3-button w3-hide-small w3-hover-white">Pricing</a>
                <a href="#contact" class="w3-bar-item w3-button w3-hide-small w3-hover-white">Contact</a>
                <div class="w3-dropdown-hover w3-hide-small">
                    <button class="w3-button" title="Notifications">Faculties <i class="fa fa-caret-down"></i></button>     
                    <div class="w3-dropdown-content w3-card-4 w3-bar-block">
                        <a href="#" class="w3-bar-item w3-button">Link</a>
                        <a href="#" class="w3-bar-item w3-button">Link</a>
                        <a href="#" class="w3-bar-item w3-button">Link</a>
                    </div>
                </div>
                <a href="register.jsp" class="w3-bar-item w3-button w3-hide-small w3-hover-white">Register</a>


                <a href="#" class="w3-bar-item w3-button w3-hide-small w3-right w3-hover-teal" title="Search"><i class="fa fa-search"></i></a>
                    <%
       if (session.getAttribute("uname") != null) {%>
                <div class="w3-dropdown-hover  w3-hide-small w3-right w3-hover-teal">
                    <button class="w3-button" title="Username">Welcome,  ${uname} <i class="fa fa-caret-down"></i></button>     
                    <div class="w3-dropdown-content w3-card-4 w3-bar-block">
                        <a href="#" class="w3-bar-item w3-button">Preferences</a>
                        <a href="#" class="w3-bar-item w3-button">Contact Support</a>
                        <a href="#" class="w3-bar-item w3-button">Logout</a>
                    </div>

                </div>
                <%
                } else {
                %>
                <a href="login.jsp" class="w3-bar-item w3-button w3-hide-small w3-right w3-hover-teal" title="Login"><i class="fa fa-user"></i></a>
                    <%
                        }

                    %>
            </div>

            <!-- Navbar on small screens -->
            <div id="navDemo" class="w3-bar-block w3-theme-d2 w3-hide w3-hide-large w3-hide-medium">
                <a href="#team" class="w3-bar-item w3-button">Apply</a>
                <a href="#work" class="w3-bar-item w3-button">About</a>
                <a href="#pricing" class="w3-bar-item w3-button">Pricing</a>
                <a href="#contact" class="w3-bar-item w3-button">Contact</a>
                <a href="#" class="w3-bar-item w3-button">Search</a>
            </div>
        </div>

