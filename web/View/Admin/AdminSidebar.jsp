<%-- 
    Document   : AdminSidebar
    Created on : Jan 15, 2018, 1:14:50 AM
    Author     : Cristi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
            <jsp:include page="Loader.jsp"/>
        <title>JSP Page</title>
    </head>
    <body>
        <div id="wrapper" class="">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
           <ul class="sidebar-nav" id="menu-accordion">
                <li class="sidebar-brand">
                    <a href="/">
                       ULBS Admin
                    </a>
                </li>
                <li class="panel dashboard"> <a data-toggle="collapse" class="collapsed" data-parent="#menu-accordion" href="#dashboard-link">Users <span class="glyphicon  arrow"></span></a>
                    <ul id="dashboard-link" class="collapse">
                        <li><a href="#" class="active">SubTest1</a></li>
                        <li><a href="#">SubTest1</a></li>
                        <li><a href="#">SubTest1</a></li>
                    </ul>
                </li>
                <li class="panel transactions"> <a data-toggle="collapse" class="collapsed" data-parent="#menu-accordion" href="#transactions-link">Universities <span class="glyphicon  arrow"></span></a>
                    <ul id="transactions-link" class="collapse">
                        <li><a href="#">SubTest2</a></li>
                        <li><a href="#">SubTest2</a></li>
                    </ul>
                </li>
                <li class="panel inv"> <a data-toggle="collapse" class="collapsed" data-parent="#menu-accordion" href="#inv-link">Faculties <span class="glyphicon  arrow"></span></a>
                    <ul id="inv-link" class="collapse">
                        <li><a href="#">SubTest2</a></li>
                        <li><a href="#">SubTest2</a></li>
                        <li><a href="#">SubTest2</a></li>
                        <li><a href="#">SubTest2</a></li>
                    </ul>
                </li>
                 <li class="panel inv"> <a data-toggle="collapse" class="collapsed" data-parent="#menu-accordion" href="#inv-link">Specialisation <span class="glyphicon  arrow"></span></a>
                    <ul id="inv-link" class="collapse">
                        <li><a href="#">SubTest2</a></li>
                        <li><a href="#">SubTest2</a></li>
                        <li><a href="#">SubTest2</a></li>
                        <li><a href="#">SubTest2</a></li>
                    </ul>
                </li>
                 <li class="panel inv"> <a data-toggle="collapse" class="collapsed" data-parent="#menu-accordion" href="#inv-link">Managers <span class="glyphicon  arrow"></span></a>
                    <ul id="inv-link" class="collapse">
                        <li><a href="#">SubTest2</a></li>
                        <li><a href="#">SubTest2</a></li>
                        <li><a href="#">SubTest2</a></li>
                        <li><a href="#">SubTest2</a></li>
                    </ul>
                </li>
                 <li class="panel inv"> <a data-toggle="collapse" class="collapsed" data-parent="#menu-accordion" href="#inv-link">Applications <span class="glyphicon  arrow"></span></a>
                    <ul id="inv-link" class="collapse">
                        <li><a href="#">SubTest2</a></li>
                        <li><a href="#">SubTest2</a></li>
                        <li><a href="#">SubTest2</a></li>
                        <li><a href="#">SubTest2</a></li>
                    </ul>
                </li>
                <li class="panel settings"> <a data-toggle="collapse" class="active" data-parent="#menu-accordion" href="#settings-link">Settings <span class="glyphicon  arrow"></span></a>
                    <ul id="settings-link" class="collapse active">
                        <li><a href="#">User Profile</a></li>
                        <li class="active"><a href="#">Company Settings</a></li>
                        <li><a href="#">Account Settings</a></li>
                        <li><a href="#">Billing Info</a></li>
                    </ul>
                </li>
            </ul>
        </div>
  