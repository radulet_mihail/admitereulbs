<%-- 
    Document   : AddSpecialisation
    Created on : Jan 3, 2018, 9:58:31 PM
    Author     : Mihail
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Partials/header.jsp"/>
<div class="space-medium">
    <div class="container">
        <div class="row">
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="account-holder">
                    <!--login-form-->
                    
                    <h3>Add Specialization</h3>
                    <br>
                    <div class="row">
                        <form  method="POST" action="../../AddSpecialization">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="name">Name<sup style="color:red">*</sup></label>
                                    <input id="name" name="name" type="text" class="form-control" placeholder="Enter name">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="idFaculty">Id Faculty<sup style="color:red">*</sup></label>
                                    <input id="idFaculty" name="idFaculty" type="text" class="form-control" placeholder="Enter IdFaculty">
                                </div>                               
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="numberOfSemester">Number of semester<sup style="color:red">*</sup></label>
                                    <input id="numberOfSemester" name="numberOfSemester" type="text" class="form-control" placeholder="Enter Number of semester">
                                </div>                               
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="hasEntryExam">Entry Exam<sup style="color:red">*</sup></label>
                                    <input id="hasEntryExam" name="hasEntryExam" type="text" class="form-control" placeholder="Entry Exam ? ">
                                </div>                               
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="sponsoredPlaces">Sponsored Places<sup style="color:red">*</sup></label>
                                    <input id="sponsoredPlaces" name="sponsoredPlaces" type="text" class="form-control" placeholder="Sponsored Places ? ">
                                </div>                               
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="paidPlaces">Paid Places<sup style="color:red">*</sup></label>
                                    <input id="paidPlaces" name="paidPlaces" type="text" class="form-control" placeholder="Paid Places ? ">
                                </div>                               
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="examDate">Exam Date<sup style="color:red">*</sup></label>
                                    <input id="examDate" name="examDate" type="date" class="form-control" placeholder="Exam Date">
                                </div>                               
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button class="btn btn-primary btn-block"> Add specialization </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--/.login-form-->
          

        </div>

    </div>
</div>

<jsp:include page="../Partials/footer.jsp"/>