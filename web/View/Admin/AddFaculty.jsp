<%-- 
    Document   : AddUniversity
    Created on : Dec 9, 2017, 8:25:29 PM
    Author     : Mihail
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="AdminSidebar.jsp"/>
 <div class="col-lg-12 main-box-container">
<div class="space-medium">
    <div class="container">
        <div class="row">
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="account-holder">
                    <!--login-form-->
                    
                    <h3>Add Faculty</h3>
                    <br>
                    <div class="row">
                        <form  method="POST" action="../../AddFaculty">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="name">Name<sup style="color:red">*</sup></label>
                                    <input id="name" name="name" type="text" class="form-control" placeholder="Enter name">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="dean">Dean<sup style="color:red">*</sup></label>
                                    <input id="dean" name="dean" type="text" class="form-control" placeholder="Enter dean">
                                </div>                               
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="address">Address<sup style="color:red">*</sup></label>
                                    <input id="address" name="address" type="text" class="form-control" placeholder="Enter address">
                                </div>                               
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="about">About</label>
                                    <input id="about" name="about" type="text" class="form-control" placeholder="Enter info">
                                </div>                               
                            </div>                            
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button class="btn btn-primary btn-block"> Add Faculty </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--/.login-form-->
          
          
        </div>

    </div>
</div>
</div>
 </div>
  </body>
</html>
