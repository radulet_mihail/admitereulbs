   


<%@page import="admitereulbs.PrivateDatas"%>
<%@page import="admitereulbs.Users"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Partials/header.jsp"/>
<div class="container">
    <div class="row profile">
		<jsp:include page="UserMenu.jsp"/>
		<div class="col-md-9">
            <div class="profile-content">
               <form method="POST" action="../Reg">
                           
                          
                            
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <% Users usrr = (Users)request.getSession().getAttribute("user");%>
                                <% PrivateDatas pd = (PrivateDatas)request.getSession().getAttribute("privateData");%>
                                <div class="form-group">
                                    <label class="control-label required" for="name"> Name<sup style="color:red">*</sup></label>
                                    <input id="name" name="name" type="text"  value=<% out.print("\""+usrr.getName()+"\""); %>  class="form-control" placeholder="Enter Your Name">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="surname">Surname<sup style="color:red">*</sup></label>
                                    <input id="surname" name="surname" type="text" value=<% out.print("\""+usrr.getSurname()+"\""); %> class="form-control" placeholder="Enter Your Surname">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="identityCode">Identity Code<sup style="color:red">*</sup></label>
                                    <input id="identityCode" name="identityCode" type="text" value=<% out.print("\""+pd.getIdentityCode()+"\""); %>   class="form-control" placeholder="Enter Your Identity Code">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="grade">Grade<sup style="color:red">*</sup></label>
                                    <input id="grade" name="grade" type="number" value=<% out.print("\""+pd.getGrade()+"\""); %>   class="form-control" placeholder="Enter Your Grade">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="admisionGrade">Admision Grade<sup style="color:red">*</sup></label>
                                    <input id="admisionGrade" name="admisionGrade" type="number" value=<% out.print("\""+pd.getAdmisionGrade()+"\""); %>   class="form-control" placeholder="Enter Your Admision Grade">
                                </div>
                            </div> 
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="isOlympic">Olympic ?<sup style="color:red">*</sup></label>
                                    <input id="isOlympic" name="isOlympic" type="checkbox" value=<% out.print("\""+pd.getIsOlympic()+"\""); %>   class="form-control" placeholder="Olympic ?">
                                </div>
                            </div>   
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="address">Address<sup style="color:red">*</sup></label>
                                    <input id="address" name="address" type="text" value=<% out.print("\""+pd.getAddress()+"\""); %>   class="form-control" placeholder="Enter Your Address">
                                </div>
                            </div>   
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="birthDate">Birthdate<sup style="color:red">*</sup></label>
                                    <input id="birthDate" name="birthDate" type="date" value=<% out.print("\""+pd.getBirthdate()+"\""); %>   class="form-control" placeholder="Enter Your Birthdate">
                                </div>
                            </div>   
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="fatherName">Father Name<sup style="color:red">*</sup></label>
                                    <input id="fatherName" name="fatherName" type="text" value=<% out.print("\""+pd.getFatherName()+"\""); %>   class="form-control" placeholder="Enter Your Father's Name">
                                </div>
                            </div>   
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="motherName">Mother Name<sup style="color:red">*</sup></label>
                                    <input id="motherName" name="motherName" type="text" value=<% out.print("\""+pd.getMotherName()+"\""); %>   class="form-control" placeholder="Enter Your Mother's Name">
                                </div>
                            </div>   
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="phoneNumber">Phone number<sup style="color:red">*</sup></label>
                                    <input id="phoneNumber" name="phoneNumber" type="text" value=<% out.print("\""+pd.getPhoneNumber()+"\""); %>   class="form-control" placeholder="Enter Your Phone number">
                                </div>
                            </div>   
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" class="btn btn-primary btn-block">Update</button>
                            </div>
                        </form>
            </div>
		</div>
	</div>
</div>
<br>
<br>
<jsp:include page="../Partials/footer.jsp"/>