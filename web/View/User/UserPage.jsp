<%-- 
    Document   : UserPage
    Created on : Dec 10, 2017, 10:00:37 PM
    Author     : Cristi
--%>

<%@page import="admitereulbs.Users"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Partials/header.jsp"/>
<div class="container">
    <div class="row profile">
		<jsp:include page="UserMenu.jsp"/>
		<div class="col-md-9">
            <div class="profile-content">
               
            </div>
		</div>
	</div>
</div>
<br>
<br>
<jsp:include page="../Partials/footer.jsp"/>
<script type="text/javascript">
function showStuff (id) 
{
    if (document.getElementById(id).style.display === "block")
    {
        document.getElementById(id).style.display = "none";
    }
    else
    {
        document.getElementById(id).style.display = "block";
    }       
}
</script>