<%-- 
    Document   : UserMenu
    Created on : Jan 13, 2018, 6:55:28 PM
    Author     : Cristi
--%>

<%@page import="admitereulbs.Users"%>
<div class="col-md-3">
			<div class="profile-sidebar">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic">
                                    <img class="img-responsive" src="../../Pictures/Devs/Cristian.jpg" alt=""/>
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
                                 
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">
						<% Users usr = (Users)request.getSession().getAttribute("user");
                                                    out.print(usr.getSurname()+" "+usr.getName());%>
					</div>
					<div class="profile-usertitle-job">
						Developer
					</div>
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
				<div class="profile-userbuttons">
					<button type="button" class="btn btn-success btn-sm">Follow</button>
					<button type="button" class="btn btn-danger btn-sm">Message</button>
				</div>
				<!-- END SIDEBAR BUTTONS -->
				<!-- SIDEBAR MENU -->
				<div class="profile-usermenu">
					<ul class="nav">
						<li class="active">
							<a href="#">
							<i class="glyphicon glyphicon-home"></i>
							Overview </a>
						</li>
                                                <li>
							<a href="#">
							<i class="glyphicon glyphicon-info"></i>
							Personal Info </a>
						</li>
						<li>
							<a href="#">
							<i class="glyphicon glyphicon-user"></i>
							Account Settings </a>
						</li>
						<li>
							<a href="#" target="_blank">
							<i class="glyphicon glyphicon-ok"></i>
							Applications </a>
						</li>
						<li>
							<a href="#">
							<i class="glyphicon glyphicon-flag"></i>
							Help </a>
						</li>
					</ul>
				</div>
				<!-- END MENU -->
			</div>
		</div>