<%-- 
    Document   : UserPage
    Created on : Dec 10, 2017, 10:00:37 PM
    Author     : Cristi
--%>

<%@page import="admitereulbs.Users"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Partials/header.jsp"/>
<div class="container">
    <div class="row profile">
		<jsp:include page="UserMenu.jsp"/>
		<div class="col-md-9">
            <div class="profile-content">
               <form method="POST" action="../Reg">
                           
                          
                            
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="password">Password<sup style="color:red">*</sup></label>
                                    <input id="email" name="password" type="password"  class="form-control" placeholder="Enter Your Password">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="newPassword">New Password<sup style="color:red">*</sup></label>
                                    <input id="email" name="newPassword" type="password"  class="form-control" placeholder="Enter Your New Password">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="repPassword">Repeat Password<sup style="color:red">*</sup></label>
                                    <input id="email" name="repPassword" type="password" class="form-control" placeholder="Reenter Your New Password">
                                </div>
                            </div>
                           
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" class="btn btn-primary btn-block">Update</button>
                            </div>
                        </form>
            </div>
		</div>
	</div>
</div>
<br>
<br>
<jsp:include page="../Partials/footer.jsp"/>