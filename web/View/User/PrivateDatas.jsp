<%-- 
    Document   : PrivateDatas
    Created on : Jan 13, 2018, 3:02:47 PM
    Author     : Mihail
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../Partials/header.jsp"/>
<div class="space-medium">
    <div class="container">
        <div class="row">
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="account-holder">
                    <!--login-form-->
                    
                    <h3>Please enter your private datas :</h3>
                    <br>
                    <div class="row">
                        <form  method="POST" action="../../AddPrivateDatas">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="identityCode">Identity Code<sup style="color:red">*</sup></label>
                                    <input id="identityCode" name="identityCode" type="text" class="form-control" placeholder="Enter Identity Code">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="grade">Grade<sup style="color:red">*</sup></label>
                                    <input id="grade" name="grade" type="text" class="form-control" placeholder="Enter your grade">
                                </div>                               
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="admisionGrade">Admision Grade<sup style="color:red">*</sup></label>
                                    <input id="admisionGrade" name="admisionGrade" type="text" class="form-control" placeholder="Enter your admision grade">
                                </div>                               
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="isOlympic">Olympic ? </label>
                                    <input id="isOlympic" name="isOlympic" type="checkbox" value = "1" class="form-control"> Olympic
                                </div>
                            </div>    
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="address">Address<sup style="color:red">*</sup></label>
                                    <input id="address" name="address" type="text" class="form-control" placeholder="Enter your address">
                                </div>                                                                
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="birthDate">BirthDate<sup style="color:red">*</sup></label>
                                    <input id="birthDate" name="birthDate" type="date" class="form-control" >
                                </div>                                                                
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="fatherName">Father Name<sup style="color:red">*</sup></label>
                                    <input id="fatherName" name="fatherName" type="text" class="form-control" placeholder="Enter your father name">
                                </div>                                                                
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="motherName">Mother Name<sup style="color:red">*</sup></label>
                                    <input id="motherName" name="motherName" type="text" class="form-control" placeholder="Enter your mother name">
                                </div>                                                                
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="phoneNumber">Phone number<sup style="color:red">*</sup></label>
                                    <input id="phoneNumber" name="phoneNumber" type="text" class="form-control" placeholder="Enter your phone number">
                                </div>                                                                
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button class="btn btn-primary btn-block"> Add datas </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--/.login-form-->
          
          
        </div>

    </div>
</div>

<jsp:include page="../Partials/footer.jsp"/>