<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="Partials/header.jsp"/>
<!-- Image Header -->
<div class="w3-display-container w3-animate-opacity">
  
  <img src="../Pictures/logoULBSblue.jpg" alt="boat" style="width:100%;min-height:350px;max-height:600px;"/>
  <div class="w3-container w3-display-bottomleft w3-margin-bottom">  
    <button onclick="document.getElementById('id01').style.display='block'" class="w3-button w3-xlarge w3-theme w3-hover-teal" title="Go To W3.CSS">APPLY</button>
  </div>
</div>

<!-- Modal -->
<div id="id01" class="w3-modal">
  <div class="w3-modal-content w3-card-4 w3-animate-top">
    <header class="w3-container w3-teal w3-display-container"> 
      <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-teal w3-display-topright"><i class="fa fa-remove"></i></span>
      <h4>Oh snap! We just showed you a modal..</h4>
      <h5>Because we can <i class="fa fa-smile-o"></i></h5>
    </header>
    <div class="w3-container">
      <p>Cool huh? Ok, enough teasing around..</p>
      <p>Go to our <a class="w3-text-teal" href="#">Will be developed</a> </p>
    </div>
    <footer class="w3-container w3-teal">
      <p> Ceva Shm3k3rye</p>
    </footer>
  </div>
</div>

<!-- Team Container -->
<div class="w3-container w3-padding-64 w3-center" id="team">
<h2>OUR TEAM</h2>
<p>Meet our team </p>

<div class="w3-row"><br>

<div class="w3-quarter">
  <img src="../Pictures/Devs/Cristian.jpg" alt="Boss" style="width:45%" class="w3-circle w3-hover-opacity">
  <h3>Eni Cristian</h3>
  <p>Web Designer</p>
</div>

<div class="w3-quarter">
  <img src="../Pictures/Devs/Alexandra.jpg" alt="Boss" style="width:45%" class="w3-circle w3-hover-opacity">
  <h3>Turcu Alexandra</h3>
  <p>Database Master</p>
</div>

<div class="w3-quarter">
  <img src="../Pictures/Devs/Mihail.jpg" alt="Boss" style="width:45%" class="w3-circle w3-hover-opacity">
  <h3>Radulet Mihail</h3>
  <p>Developer</p>
</div>

<div class="w3-quarter">
  
  <img src="../Pictures/Devs/Anamaria.jpg" alt="Boss" style="width:45%" class="w3-circle w3-hover-opacity">
  <h3>Corlaci Anamaria</h3>
  <p>Fixer</p>
</div>

</div>
</div>

<!-- Work Row -->
<div class="w3-row-padding w3-padding-64 w3-theme-l1" id="work">

<div class="w3-quarter">
<h2>Our Faculties</h2>
<p>In a highly interconnected and interactive world, LBUS could keep in the mainstream of academic events only by promoting international cooperation. After 1992, the year which marked the beginning of Dr. Ciocoi-Pop’s administration, LBUS adopted a policy of unprecedented academic contacts and links. It was the leadership’s strong conviction that, among the forty-eight state universities, eight military academies and forty-nine private universities of Post-Revolutionary Romania, LBUS could be successful only by interaction and cooperation.</p>
</div>

<div class="w3-quarter">
<div class="w3-card w3-white">

  <img src="../Pictures/Faculties/engineering.jpg" alt="Engineering" style="width:100%">
  <div class="w3-container">
  <h3>Faculty of Engineering</h3>
  <h4>About</h4>
  <p style="align:justify;">The Hermann Oberth Faculty of Engineering is a polytechnic school with a long tradition and great experience which have made it into one of themost important Faculties of Engineering in Romania.<br>Nowadays, it is a functional unit of the Lucian Blaga University of Sibiu. It was legally acknowledged by Act no 88/1993 by the Ministry of Education and Research.</p>

  </div>
  </div>
</div>

<div class="w3-quarter">
<div class="w3-card w3-white">

  <img src="../Pictures/Faculties/letters.jpg" alt="Letters" style="width:100%">
  <div class="w3-container">
  <h3>Faculty of Letters</h3>
  <h4>About</h4>
  <p>Higher education in the Humanities was established in Sibiu in 1969 under the stimulating patronage of “Babes-Bolyai” University of Cluj-Napoca. </p><p>In the period before 1990 it produced 15 generations of alumni and, after a 3-year discontinuity between 1987-1990, it re-emerged under the aegis of the newly founded University of Sibiu, named after the distinguished national philosopher and scholar Lucian Blaga in 1995.</p>

  </div>
  </div>
</div>

<div class="w3-quarter">
<div class="w3-card w3-white">
    
  <img src="../Pictures/Faculties/sciences.jpg" alt="Science" style="width:100%">
  <div class="w3-container">
  <h3>Faculty of Science</h3>
  <h4>About</h4>
<p>he Faculty of Sciences, through its variety of domains of study which it offers, has a special place in "Lucian Blaga" University of Sibiu, assuring a high educational standard, developed and improved through complex research activity, in line with the latest global body of knowledge.

The Faculty of Sciences offers programmes of study at Degree and Master level, in the following fields of specialization: Mathematics, Informatics, Environmental Sciences, Physics, Biology. </p>
  </div>
  </div>
</div>

</div>

<!-- Container -->
<div class="w3-container" style="position:relative">
  <a onclick="w3_open()" class="w3-button w3-xlarge w3-circle w3-teal"
  style="position:absolute;top:-28px;right:24px">+</a>
</div>

<!-- Pricing Row -->
<div class="w3-row-padding w3-center w3-padding-64" id="pricing">
    <h2>PRICING</h2>
    <p>Choose the Faculty you want to apply !</p><br>
        <div class="w3-third w3-margin-bottom">
      <ul class="w3-ul w3-border w3-hover-shadow">
        <li class="w3-theme">
          <p class="w3-xlarge">Engineering</p>
        </li>
        <li class="w3-padding-16"><b>125</b> Places</li>
        <li class="w3-padding-16"><b>48</b> Teachers</li>
        <li class="w3-padding-16"><b>12900+</b> Graduated</li>
        <li class="w3-padding-16">
          <h2 class="w3-wide"><i class="fa fa-usd"></i> 120</h2>
          <span class="w3-opacity">per student</span>
        </li>
        <li class="w3-theme-l5 w3-padding-24">
          <button class="w3-button w3-teal w3-padding-large"><i class="fa fa-check"></i> Apply</button>
        </li>
      </ul>
    </div>

    <div class="w3-third w3-margin-bottom">
      <ul class="w3-ul w3-border w3-hover-shadow">
        <li class="w3-theme-l2">
          <p class="w3-xlarge">Letters</p>
        </li>
        <li class="w3-padding-16"><b>85</b> Places</li>
        <li class="w3-padding-16"><b>32</b> Teachers</li>
        <li class="w3-padding-16"><b>12100+</b> Graduated</li>
        <li class="w3-padding-16">
          <h2 class="w3-wide"><i class="fa fa-usd"></i> 80</h2>
          <span class="w3-opacity">per student</span>
        </li>
        <li class="w3-theme-l5 w3-padding-24">
          <button class="w3-button w3-teal w3-padding-large"><i class="fa fa-check"></i> Apply</button>
        </li>
      </ul>
    </div>

          <div class="w3-third w3-margin-bottom">
      <ul class="w3-ul w3-border w3-hover-shadow">
        <li class="w3-theme">
          <p class="w3-xlarge">Sciences</p>
        </li>
        <li class="w3-padding-16"><b>55</b> Places</li>
        <li class="w3-padding-16"><b>28</b> Teachers</li>
        <li class="w3-padding-16"><b>9000+</b> Graduated</li>
        <li class="w3-padding-16">
          <h2 class="w3-wide"><i class="fa fa-usd"></i> 85</h2>
          <span class="w3-opacity">per student</span>
        </li>
        <li class="w3-theme-l5 w3-padding-24">
          <button class="w3-button w3-teal w3-padding-large"><i class="fa fa-check"></i> Apply</button>
        </li>
      </ul>
    </div>
</div>

<!-- Contact Container -->
<div class="w3-container w3-padding-64 w3-theme-l5" id="contact">
  <div class="w3-row">
    <div class="w3-col m5">
    <div class="w3-padding-16"><span class="w3-xlarge w3-border-teal w3-bottombar">Contact Us</span></div>
      <h3>Address</h3>
      <p></p>
      <p><i class="fa fa-map-marker w3-text-teal w3-xlarge"></i> : Bd-ul. Victoriei, Nr.10, Sibiu, 550024, România</p>
      <p><i class="fa fa-phone w3-text-teal w3-xlarge"></i> +40-(269) 21.60.62</p>
      <p><i class="fa fa-envelope-o w3-text-teal w3-xlarge"></i>  rectorat@ulbsibiu.ro</p>
    </div>
    <div class="w3-col m7">
      <form class="w3-container w3-card-4 w3-padding-16 w3-white" action="/action_page.php" target="_blank">
      <div class="w3-section">      
        <label>Name</label>
        <input class="w3-input" type="text" name="Name" required>
      </div>
      <div class="w3-section">      
        <label>Email</label>
        <input class="w3-input" type="text" name="Email" required>
      </div>
      <div class="w3-section">      
        <label>Message</label>
        <input class="w3-input" type="text" name="Message" required>
      </div>  
      <input class="w3-check" type="checkbox" checked name="Like">
      <label>I Love it!</label>
      <button type="submit" class="w3-button w3-right w3-theme">Send</button>
      </form>
    </div>
  </div>
</div>

<jsp:include page="Partials/footer.jsp"/>