<%-- 
    Document   : register
    Created on : Nov 18, 2017, 1:49:30 PM
    Author     : Cristi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="Partials/header.jsp"/>
<div class="space-medium">
    <div class="container">
        <div class="row">
  <!--sing-up-form-->
            <div class="offset-lg-3 offset-md-3  offset-sm-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="account-holder">
                    <h3>Signup </h3>
                    <br>
                    <div class="social-btn">
                        <h6>Sign up With</h6>
            
                        <div class="fb-btn">
                            <i class="fa fa-facebook-official"></i><a href="#" class="fb-btn-text">facebook</a>
                        </div>
                        <div class="google-btn">
                            <i class="fa fa-google"></i><a href="#" class="google-btn-text">Google</a>
                        </div>
                    </div>
                    <div class="row">
                        <form method="POST" action="../Reg">
                           
                          
                            
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="name"> Name<sup style="color:red">*</sup></label>
                                    <input id="name" name="name" type="text" class="form-control" placeholder="Enter Your Name">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="surname">Surname<sup style="color:red">*</sup></label>
                                    <input id="surname" name="surname" type="text" class="form-control" placeholder="Enter Your Surname">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="email">Email<sup style="color:red">*</sup></label>
                                    <input id="email" name="email" type="text" class="form-control" placeholder="Enter Email Address">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="password">Password<sup style="color:red">*</sup></label>
                                    <input id="email" name="password" type="password" class="form-control" placeholder="Enter Your Password">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label required" for="repPassword">Repeat Password<sup style="color:red">*</sup></label>
                                    <input id="email" name="repPassword" type="password" class="form-control" placeholder="Reenter Your Password">
                                </div>
                            </div>
              

                                <div class="mb30">
                                    <p>Already have an account?   <a href="#">Login</a></p>
                                </div>
                           
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" class="btn btn-primary btn-block">Register</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!--/.sing-up-form-->
        </div>

    </div>
</div>

<jsp:include page="Partials/footer.jsp"/>